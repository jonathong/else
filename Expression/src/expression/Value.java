/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents a ValueNumber or a ValueVector
 */
public class Value extends Instruction {

    Value() {
    }
    @Override
    Value eval() {
        return this;
    }

    static Value parse() {
        Token t = scanner.getCurrentToken();
        boolean negative = false;
        if (t instanceof MinusOpToken) {
            negative = true;
            t = scanner.get();
        }
        if (t instanceof NumberToken ){
            Number no = ((NumberToken) t).getNumber();
            double n = no.doubleValue();
            if (negative) n = -n;
            return new ValueNumber(n);
        }else if(t instanceof IdToken){
            String word = ((IdToken) t).getValue();
            Value v = env_table.get(word);
//            if(v instanceof ValueNumber)
//                return new ValueNumber( v.getValue(), negative );
//            if(v instanceof ValueVector)
//                return new ValueVector( ((ValueVector) v).getValues(), negative );
            return v;
        }else if(t instanceof LeftBracketToken){
            ArrayList<Expression> values = new ArrayList<>(3);
            while(!(t instanceof RightBracketToken)){
                t =scanner.get();
                values.add( Expression.parse() );
                t =scanner.getCurrentToken();
            }
            t = scanner.get();
            return  new ValueVector(values);
        }
        Log.fatal("returning null after attempting to parse a value");
        return null;
    }

   @Override
   void print() {

   }
    public double mag(){
        return 0;
    }
    public double getValue(){
        return 0;
    }
    public ArrayList<Instruction> getValues(){
        ArrayList<Instruction> res = new ArrayList<>();
        res.add( new ValueNumber(0) );
        return res;
    }
    public int size(){
        return 0;
    }
}

/**
 * A ValueNumber can be any real number. i.e. 4 or 3.14.
 */
class ValueNumber extends Value {
    private double num;
    private boolean sign;
    public ValueNumber(double n) {
        this(n, false);
    }
    public ValueNumber(double n, boolean s){
        num = n;
        sign = s;
    }

    @Override
    Value eval(){
        if(sign) {
            sign = false;
            return new ValueNumber(0-num);
        }
        return this;}

    public double getValue(){
        return num;
    }
    public ArrayList<Instruction> getValues(){
        ArrayList<Instruction> res = new ArrayList<>();
        res.add( new ValueNumber(num) );
        return res;
    }
    public String toString() {
        return num+"";
    }

    public double mag(){
        return num;
    }
    public int size(){
        return 1;
    }

}

/**
 * Represents a list of ValueNumbers
 */
class ValueVector extends Value {
    private boolean sign = false;
    private ArrayList<Instruction> components = new ArrayList();
    public ValueVector(ArrayList c) {
        this(c, false);
    }
    public ValueVector(ArrayList c, boolean s){
        this.components = c;
        if(components.isEmpty()){
            components.add(new ValueNumber(0));
        }
        sign = s;
    }

    @Override
    Value eval(){
        if(sign){
            sign = false;
            ArrayList<ValueNumber> res = new ArrayList();
            for(Instruction i : components){
                res.add(new ValueNumber(0-i.eval().getValue()));
            }
            return new ValueVector(res);
        }
        return this;
    }
    public double getValue(int i) {
        if (i >= this.size()){
            return 0;
        }
        return components.get(i).eval().getValue();
    }
    public ArrayList<Instruction> getValues(){
        return components;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(components.size()+2);
        sb.append('[');
        for(Instruction i : components){
            sb.append(i.eval().getValue());
            sb.append(",");
        }
        sb.append(']');
        return sb.toString();
    }

    public double mag() {
        double sum = 0;
        for (Instruction e : components) {
            sum = sum + Math.pow(e.eval().getValue(), 2);
        }
        return Math.sqrt(sum);
    }
    public int size(){
        return components.size();
    }
}
