/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package expression;

/**
 *
 * @author morell
 * <Expression>::= <ExpTerm> || <ExpAddEnd>
 */
public class Expression extends Instruction{

    Expression(){
        
    }


    Value eval() {
        return new ValueNumber(0);
    }
    
    static Expression parse() {
        Term term;
        Expression result;
        AddEnd addend;
        term = Term.parse();
        Token t = scanner.getCurrentToken();
        if (t instanceof PlusOpToken || t instanceof MinusOpToken) {
            addend = (AddEnd) AddEnd.parse();
            result = new ExpAddEnd(term, addend);
        } else {
            result = new ExpTerm(term);
        }
        //now that we have an expression built lets see if this is a boolean expression
        if (t instanceof BooleanToken) {
            scanner.get();
            result = new ExpBoolean(t, result, Expression.parse());
        }
        return result;
    }

    @Override
    void print() {
        //to do
    }
   
}

class ExpTerm extends Expression {
    Term term;
    
    ExpTerm(Term te){
        this.term = te;
    }
    Value eval() {
        return term.eval();
    }

    @Override
    void print() {
        term.print();
    }
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(term.toString());
        return sb.toString();
    }
    
}

class ExpAddEnd extends Expression {
    Term t;
    AddEnd ae;
    
    ExpAddEnd(Term t, AddEnd a){
        this.t = t;
        this.ae = a;
    }
    Value eval() {
        Value left = t.eval();
        return ae.eval(left);
    }

    @Override
    void print() {
        t.print(); ae.print();
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(t.toString());
        sb.append(ae.toString());
        return sb.toString();
   }
    
}

class ExpBoolean extends Expression{
    Expression left;
    Expression right;
    Token token;

    ExpBoolean(Token t, Expression l, Expression r){
        left = l;
        right = r;
        token = t;
    }

    Value eval() {
        double leftMag = left.eval().mag();
        double rightMag = right.eval().mag();
        if(token instanceof GreaterThanOpToken){
            if(leftMag > rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof LessThanOpToken){
            if(leftMag < rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof GreaterThanEqualOpToken){
            if(leftMag >= rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof LessThanEqualOpToken){
            if(leftMag <= rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof EqualOpToken) {
            if (leftMag == rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof NotEqualOpToken){
            if(leftMag != rightMag)
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof AndOpToken){
            if( (leftMag>0) && (rightMag>0) )
                return new ValueNumber(1);
            return new ValueNumber(0);

        }else if(token instanceof OrOpToken){
            if( (leftMag>0) || (rightMag>0) )
                return new ValueNumber(1);
            return new ValueNumber(0);
        }
        else{
            Log.err("ExpBoolean#eval, token does not match any boolean tokens, token = "+token.getValue() );
        }
        return null;
    }

    @Override
    void print() {
        left.print();System.out.print("");right.print();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(left.toString());
        sb.append(token.toString());
        sb.append(right.toString());
        return sb.toString();
    }
}