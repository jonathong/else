/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import java.util.ArrayList;

/**
 *  Handles adddition and subtraction and differentiates between
 *  data types.
 * @author Jonathon Gray
 */
public class AddOp {

   char operator;


   Value eval(Value l, Value r) {
      return new ValueNumber(0);
   }

   char get() {
      return 'c';
   }

   void print() {
      System.out.print(operator);
   }
}

class AddOpMinus extends AddOp {

   AddOpMinus() {
      operator = '-';
   }

   char get() {
      return operator;
   }

    /**
     * eval() determines the left and right types and adds accordingly.
     * If the values are both vectors, then the right elements are removed
     * for the left.
     * If there is one parameter that is a ValueNumber then the number is
     * subtracted from each element of the list.
     * If both are numbers,then normal subtraction is executed.
     * @param left
     * @param right
     * @return Value
     */
    Value eval(Value left, Value right) {
        if(left instanceof ValueVector && right instanceof ValueVector){
            ArrayList<Instruction> l = left.eval().getValues();
            ArrayList<Instruction> r = right.eval().getValues();
            ArrayList<Instruction> res = new ArrayList<>();
            for (Instruction aL : l) {
                boolean found = false;
                for (Instruction aR : r) {
                    if (aL.eval().getValue() == aR.eval().getValue())
                        found = true;
                }
                if (!found)
                    res.add(aL);
            }
            return new ValueVector( res );
        }else if(right instanceof ValueVector){ // right is a vector/list
            ArrayList<ValueNumber> res = new ArrayList<>(right.size());
            ArrayList<Instruction> r = right.eval().getValues();
            for(Instruction i : r){
                res.add( new ValueNumber(left.getValue()-i.eval().getValue()));
            }
            return new ValueVector(res);
        }else if(left instanceof ValueVector){ //left is a vector/list
            ArrayList<ValueNumber> res = new ArrayList<>(right.size());
            ArrayList<Instruction> l = left.eval().getValues();
            for(Instruction i : l){
                res.add( new ValueNumber(i.eval().getValue() - right.eval().getValue()));
            }
            return new ValueVector(res);

        }else{
            return new ValueNumber(left.eval().getValue() - right.eval().getValue());
        }
    }

}

class AddOpPlus extends AddOp {

   AddOpPlus() {
      operator = '+';
   }

   char get() {
      return operator;
   }


    /**
     * eval() determines the left and right types and adds accordingly.
     * If the values are both vectors, then the two are concatenated.
     * If there is one parameter that is a ValueNumber then the number is
     * added to each element of the list.
     * If both are numbers,then normal addition is executed.
     * @param left
     * @param right
     * @return Value
     */
   Value eval(Value left, Value right) {

       if(left instanceof ValueVector && right instanceof ValueVector){
           ArrayList<Instruction> l = left.eval().getValues();
           ArrayList<Instruction> r = right.eval().getValues();
           ArrayList<Instruction> res = new ArrayList<>();
           res.addAll(l);
           res.addAll(r);
           return new ValueVector( res );
       }else if(right instanceof ValueVector){ // right is a vector/list
           ArrayList<ValueNumber> res = new ArrayList<>(right.size());
           ArrayList<Instruction> r = right.eval().getValues();
           for(Instruction i : r){
               res.add( new ValueNumber(left.getValue()+i.eval().getValue()));
           }
           return new ValueVector(res);
       }else if(left instanceof ValueVector){ //left is a vector/list
           ArrayList<ValueNumber> res = new ArrayList<>(right.size());
           ArrayList<Instruction> l = left.eval().getValues();
           for(Instruction i : l){
               res.add( new ValueNumber(i.eval().getValue() + right.getValue()));
           }
           return new ValueVector(res);

       }else{
           return new ValueNumber(left.eval().getValue() + right.eval().getValue());
       }
   }

}

