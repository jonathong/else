/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 *
 * @author morell
 */
public abstract class Instruction {
   static Scanner scanner;  // Lazy
   static protected Environment env_table = new Environment(); // user variable Environment
   static protected FunctionEnvironment fenv_table = new FunctionEnvironment(); // Math Function Env
   static Logger Log = new Logger();
   static void setScanner(Scanner s) { scanner = s; }
   static Instruction parse(){return null;}
   Value eval() {return new ValueNumber(0);}
   abstract void print();
}
