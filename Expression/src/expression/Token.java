package expression;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * The Token class associates strings with their source (filename, line, pos)
 * and symbol, and classifies them into one of the following:
 * <ul>
 * <li>&lt;id> -- a char followed by one or more chars, digits, or
 * underscores</li>
 * <li>&lt;no -- a typical floating point of integer literal </li>
 * <li>&lt;string -- a quote (single or double) followed by zero or more
 * intervening chars followed by a matching quote (single or double; special
 * characters must be escaped by a backslash. These include:
 * <ul>
 * <li>a backslash </li>
 * <li>a double quote (if the opening quote is a double quote)</li>
 * <li>a single quote (if the opening quote is a single quote)</li>
 * <li>a newline</li>
 * </ul> 
 * </li>
 * <li>&lt;char> </li>
 * <li>&lt;operator</li>
 * </ul>
 *
 * @author Larry Morell <morell@cs.atu.edu>
 */
public class Token {
    
    //-----------------  Member variables -----------------//
    protected String value;         // The string extracted from the buffer
    protected Buffer buffer;        // The data source for the string
    protected String fileName;      // the name of the file from whence this token was extracted
    protected int lineNo;           // the line number in the source file
    protected int charPos;          // the character position in the source file

    //--------------------- Constructors: none -----------------//
    // Setters
    public void setBuffer(Buffer b) {
        buffer = b;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public void setCharPos(int charPos) {
        this.charPos = charPos;
    }

    public int getCharPos() {
        return charPos;
    }

    public String getFileName() {
        return fileName;
    }

    public int getLineNo() {
        return lineNo;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String v) {
        value = v;
    }
    
    @Override
    public String toString() {
        return value;
    }

    Token get() {
        buffer.get(); // Toss the operator
        return this;
    }
}

class BadToken extends Token {

    BadToken() {
        value = "";
    }

    BadToken(String chars) {
        value = chars;
    }
}

class EofToken extends Token {

    EofToken() {
        value = "eof";
    }

}

class IdToken extends Token { // Not used ... yet!
    private static HashMap<String,Token> keywords;
   
    public IdToken() {
        //setValue("");
        //value = get();
    }

    static HashMap getKeywords(){
        return keywords;
    }

    public IdToken(String v) {
        this.value = v;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName();
    }

    IdToken newInstance() {
        return new IdToken("");
    }
    
    static public void registerKeyword(String word, Token token){
        if(keywords==null)
            keywords = new HashMap<>();
        keywords.put(word, token);
    }
    
    public static boolean isKeyword(String word){
        boolean result = false;
        if( keywords.containsKey(word) )
            result = true;
        return result;
    }
    
    //Returns either a IdToken or a keyword's token.
    @Override
    Token get(){
        String name = buffer.getId();
        if( keywords.containsKey(name) ){
            return keywords.get(name);
        }
        else{
            IdToken id = new IdToken(name);
            return id;
        }
    }
    
}

class LeftParenToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static LeftParenToken single;
    public static LeftParenToken getLeftParenToken(){
        if(single==null)
            single = new LeftParenToken();
    return single;
    }
     private LeftParenToken() {
        value = "(";
    }

}

class ParamIterOpToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static ParamIterOpToken single;
    public static ParamIterOpToken getParamIterOpToken(){
        if(single==null)
            single = new ParamIterOpToken();
        return single;
    }
    private ParamIterOpToken() {
        value = ",";
    }

}

class StartBraceToken extends Token {

    private static StartBraceToken single;
    public static StartBraceToken getStartBraceToken(){
        if(single==null)
            single = new StartBraceToken();
        return single;
    }
    private StartBraceToken() {
        value = "{";
    }

}

class EndBraceToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static EndBraceToken single;
    public static EndBraceToken getEndBraceToken(){
        if(single==null)
            single = new EndBraceToken();
        return single;
    }
    private EndBraceToken() {
        value = "{";
    }

}

class AssignmentOpToken extends Token{
    private static AssignmentOpToken single;
    
    private AssignmentOpToken(){
        value = ":=";
    }

    @Override
    Token get(){
        buffer.get();
        if(buffer.peek() == '='){
            buffer.get();
            return new AssignmentOpToken();
        }else{
            System.out.println("Error: expected '=' following : for assignment operation\n");
        }
        return null;
    }

    public static AssignmentOpToken getAssignmentOpToken(){
        if(single == null)
            single = new AssignmentOpToken();
        return single;
    }
}

class MinusOpToken extends Token {
    private static MinusOpToken single;
    
    public static MinusOpToken getMinusOpToken(){
        if(single == null)
            single = new MinusOpToken();
    return single;
    }
    
    private MinusOpToken() {
        value = "-";
    }

}

class PlusOpToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static PlusOpToken single;
    public static PlusOpToken getPlusOpToken(){
        if(single == null)
            single = new PlusOpToken();
    return single;
    }
    private PlusOpToken() {
        value = "+";
    }

}

class DivideOpToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static DivideOpToken single;
    public static DivideOpToken getDivideOpToken(){
        if(single == null)
            single = new DivideOpToken();
    return single;
    }
    
    private DivideOpToken() {
        value = "/";
    }

}

class MultOpToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static MultOpToken single;
    public static MultOpToken getMultOpToken(){
        if(single == null)
            single = new MultOpToken();
    return single;
    }
    
    private MultOpToken() {
        value = "*";
    }

}
class ModulusOpToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static ModulusOpToken single;
    public static ModulusOpToken getModulusOpToken(){
        if(single == null)
            single = new ModulusOpToken();
        return single;
    }

    private ModulusOpToken() {
        value = "%";
    }

}
class PowerOpToken extends Token{

    private static PowerOpToken single;
    public static PowerOpToken getPowerOpToken(){
        if(single == null)
            single = new PowerOpToken();
        return single;
    }

    private PowerOpToken() {
        value = "^";
    }
}

class NumberToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static NumberToken single;
    public static NumberToken getNumberToken(){
        if(single==null)
            single = new NumberToken();
        return single;
    }
    public static NumberToken getNumberToken(String value){
        if(single == null){
            single = new NumberToken(value);
        }
        else{
            single.value = value;
            if (value.contains(".")) {
                single.number = Double.parseDouble(value);
            } else {
                single.number = Integer.parseInt(value);
            }
        }
        return single;
    }
    
    
    Number number;

    private NumberToken() {
        value = "0";
        number = 0;
    }

    private NumberToken(String value) {
        this.value = value;
        if (value.contains(".")) {
            this.number = Double.parseDouble(value);
        } else {
            this.number = Integer.parseInt(value);
        }
    }

    @Override
    public Token get() {
        String n = buffer.getNumber();
        NumberToken no = getNumberToken(n);
        no.setBuffer(buffer);
        return no;
    }

    public String getValue() {
        return value;
    }

    public Number getNumber() {
        return number;
    }
}

class RightParenToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static RightParenToken single;
    public static RightParenToken getRightParenToken(){
        if(single == null)
            single = new RightParenToken();
    return single;
    }
    
    private RightParenToken() {
        value = ")";
    }

}

class LeftBracketToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static LeftBracketToken single;
    public static LeftBracketToken getLeftBracketToken(){
        if(single == null)
            single = new LeftBracketToken();
        return single;
    }
    private LeftBracketToken() {
        value = "[";
    }
}

class RightBracketToken extends Token {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static RightBracketToken single;

    public static RightBracketToken geRighttBracketToken() {
        if (single == null)
            single = new RightBracketToken();
        return single;
    }

    private RightBracketToken() {
        value = "]";
    }
}

class StringToken extends Token { // Not used ... yet

    StringToken(String s) {
        value = s;
    }
}


/*
*  By encompassing each token that represents a statement we can more easily decide if a
*  certain Token is indeed a new statement.
 */
class StatementToken extends Token {
}

class PrintToken extends StatementToken {
    private static PrintToken single;
    
    public static PrintToken getPrintToken(){
        if(single == null){
            single = new PrintToken();
        }
    return single;
    }
    
    private PrintToken() {
        value = "print";
    }
}

class MakeToken extends StatementToken {
    private static MakeToken single;

    public static MakeToken getMakeToken(){
        if(single == null){
            single = new MakeToken();
        }
        return single;
    }

    private MakeToken() {
        value = "make";
    }
}

class IfToken extends StatementToken {
    private static IfToken single;

    public static IfToken getIfToken(){
        if(single == null){
            single = new IfToken();
        }
        return single;
    }

    private IfToken() {
        value = "if";
    }
}
class ElseToken extends StatementToken {
    private static ElseToken single;

    public static ElseToken getElseToken(){
        if(single == null){
            single = new ElseToken();
        }
        return single;
    }

    private ElseToken() {
        value = "else";
    }
}
class WhileToken extends StatementToken {
    private static WhileToken single;

    public static WhileToken getWhileToken(){
        if(single == null){
            single = new WhileToken();
        }
        return single;
    }

    private WhileToken() {
        value = "while";
    }
}
class DoWhileToken extends StatementToken {
    private static DoWhileToken single;

    public static DoWhileToken getDoWhileToken(){
        if(single == null){
            single = new DoWhileToken();
        }
        return single;
    }

    private DoWhileToken() {
        value = "do";
    }
}

/**
 * To incorporate functions into the language we need an easy way to determine
 * if the token is a function.  This is simple if we extend each Token with
 * FunctionToken.  Then we can check if the Token is of type FunctionToken rather
 * than checking if it is one of several tokens.
 * @author Jonathon Gray
 */
class FunctionToken extends Token{
}
class RootToken extends FunctionToken{
    private static RootToken single;

    public static RootToken getRootToken(){
        if(single == null){
            single = new RootToken();
        }
        return single;
    }
    private RootToken(){value = "root"; }
}
class MagToken extends FunctionToken{
    private static MagToken single;

    public static MagToken getMagToken(){
        if(single == null){
            single = new MagToken();
        }
        return single;
    }
    private MagToken(){value = "mag"; }
}
class SizeOfToken extends FunctionToken{
    private static SizeOfToken single;

    public static SizeOfToken getSizeOfToken(){
        if(single == null){
            single = new SizeOfToken();
        }
        return single;
    }
    private SizeOfToken(){value = "sizeOf"; }
}
class CosToken extends FunctionToken{
    private static CosToken single;

    public static CosToken getCosToken(){
        if(single == null){
            single = new CosToken();
        }
        return single;
    }
    private CosToken(){value = "cos"; }
}
class SinToken extends FunctionToken{
    private static SinToken single;

    public static SinToken getSinToken(){
        if(single == null){
            single = new SinToken();
        }
        return single;
    }
    private SinToken(){value = "sin"; }
}
class MeanToken extends FunctionToken{
    private static MeanToken single;

    public static MeanToken getMeanToken(){
        if(single == null){
            single = new MeanToken();
        }
        return single;
    }
    private MeanToken(){value = "mean"; }
}
class SamVarianceToken extends FunctionToken{
    private static SamVarianceToken single;

    public static SamVarianceToken getSamVarianceToken(){
        if(single == null){
            single = new SamVarianceToken();
        }
        return single;
    }
    private SamVarianceToken(){value = "sVar"; }
}
class PopVarianceToken extends FunctionToken{
    private static PopVarianceToken single;

    public static PopVarianceToken getPopVarianceToken(){
        if(single == null){
            single = new PopVarianceToken();
        }
        return single;
    }
    private PopVarianceToken(){value = "pVar"; }
}
class PopStdDevToken extends FunctionToken{
    private static PopStdDevToken single;

    public static PopStdDevToken getPopStdDevToken(){
        if(single == null){
            single = new PopStdDevToken();
        }
        return single;
    }
    private PopStdDevToken(){value = "pStdDev"; }
}
class SamStdDevToken extends FunctionToken{
    private static SamStdDevToken single;

    public static SamStdDevToken getSamStdDevToken(){
        if(single == null){
            single = new SamStdDevToken();
        }
        return single;
    }
    private SamStdDevToken(){value = "sStdDev"; }
}
class PopCorrToken extends FunctionToken{
    private static PopCorrToken single;

    public static PopCorrToken getPopCorrToken(){
        if(single == null){
            single = new PopCorrToken();
        }
        return single;
    }
    private PopCorrToken(){value = "pCorr"; }
}
class SamCorrToken extends FunctionToken{
    private static SamCorrToken single;

    public static SamCorrToken getSamCorrToken(){
        if(single == null){
            single = new SamCorrToken();
        }
        return single;
    }
    private SamCorrToken(){value = "sCorr"; }
}
class CorrToken extends FunctionToken{
    private static CorrToken single;

    public static CorrToken getCorrToken(){
        if(single == null){
            single = new CorrToken();
        }
        return single;
    }
    private CorrToken(){value = "corr"; }
}
class CrossToken extends FunctionToken{
    private static CrossToken single;

    public static CrossToken getCrossToken(){
        if(single == null){
            single = new CrossToken();
        }
        return single;
    }
    private CrossToken(){value = "cross"; }
}
class DotToken extends FunctionToken{
    private static DotToken single;

    public static DotToken getDotToken(){
        if(single == null){
            single = new DotToken();
        }
        return single;
    }
    private DotToken(){value = "dot"; }
}

/*
*  BooleanToken is the parent class of each Boolean operator.  This way we can check if a Token is
*  a boolean token instead of checking each type.
 */
class BooleanToken extends Token{
}

class GreaterThanOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static GreaterThanOpToken single;
    public static GreaterThanOpToken getGreaterThanOpToken(){
        if(single == null)
            single = new GreaterThanOpToken();
        return single;
    }

    private GreaterThanOpToken() {
        value = ">";
    }

}
class LessThanOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static LessThanOpToken single;
    public static LessThanOpToken getLessThanOpToken(){
        if(single == null)
            single = new LessThanOpToken();
        return single;
    }

    private LessThanOpToken() {
        value = "<";
    }

}
class EqualOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static EqualOpToken single;
    public static EqualOpToken getEqualOpToken(){
        if(single == null)
            single = new EqualOpToken();
        return single;
    }
    @Override
    Token get(){
        buffer.get();
        if(buffer.peek()=='='){
            buffer.get();
            return getEqualOpToken();
        }else {
            System.out.println("Error: expected '=' following = for equal operation\n");
        }
        return null;
    }
    private EqualOpToken() {
        value = "==";
    }

}
class NotEqualOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static NotEqualOpToken single;
    public static NotEqualOpToken getNotEqualOpToken(){
        if(single == null)
            single = new NotEqualOpToken();
        return single;
    }
    @Override
    Token get(){
        buffer.get();
        if(buffer.peek()=='='){
            buffer.get();
            return getNotEqualOpToken();
        }else {
            System.out.println("Error: expected '=' following ! for not equal operation\n");
        }
        return null;
    }
    private NotEqualOpToken() {
        value = "!=";
    }

}
class GreaterThanEqualOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static GreaterThanEqualOpToken single;
    public static GreaterThanEqualOpToken getGreaterThanEqualOpToken(){
        if(single == null)
            single = new GreaterThanEqualOpToken();
        return single;
    }
    @Override
    Token get(){
        char c = buffer.get();
        if(buffer.peek()=='='){
            buffer.get();
            return getGreaterThanEqualOpToken();
        }else  {
            return GreaterThanOpToken.getGreaterThanOpToken();
        }

    }

    private GreaterThanEqualOpToken() {
        value = ">=";
    }

}
class LessThanEqualOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static LessThanEqualOpToken single;
    public static LessThanEqualOpToken getLessThanEqualOpToken(){
        if(single == null)
            single = new LessThanEqualOpToken();
        return single;
    }
    @Override
    Token get(){
        char c = buffer.get();
        if(buffer.peek()=='='){
            buffer.get();
            return getLessThanEqualOpToken();
        }else  {
            return LessThanOpToken.getLessThanOpToken();
        }

    }

    private LessThanEqualOpToken() {
        value = "<=";
    }

}
class OrOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static OrOpToken single;
    public static OrOpToken getOrOpToken(){
        if(single == null)
            single = new OrOpToken();
        return single;
    }

    @Override
    Token get(){
        buffer.get();
        if(buffer.peek()=='|'){
            buffer.get();
            return getOrOpToken();
        }else {
            System.out.println("Error: expected '|' following | for or operation\n");
        }
        return null;
    }


    private OrOpToken() {
        value = "||";
    }

}
class AndOpToken extends BooleanToken {
    //Jonathon Gray
    //Added for singleton pattern, only one object will ever be created.
    private static AndOpToken single;
    public static AndOpToken getAndOpToken(){
        if(single == null)
            single = new AndOpToken();
        return single;
    }

    @Override
    Token get(){
        buffer.get();
        if(buffer.peek()=='&'){
            buffer.get();
            return getAndOpToken();
        }else {
            System.out.println("Error: expected '&' following & for and operation\n");
        }
        return null;
    }

    private AndOpToken() {
        value = "&&";
    }
}

class GraphToken extends Token {
    private static GraphToken single;
    public  static GraphToken getGraphToken(){
        if(single == null)
            single = new GraphToken();
        return single;
    }
    private GraphToken(){
        value = "graph";
    }
}