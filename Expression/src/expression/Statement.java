/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.*;

/**
 *<Statement>  ::= <Assignment> | <Print> | <AssignElement> | <DoWhile>
 *               | <FunctionAssignment> | <If> | <Make> | <While>
 *
 * @author Jonathon
 */
public class Statement extends Instruction{
    
    Statement(){
        
    }
    
    Value eval() {  /// override this
        return new ValueNumber(0);
    }

    /**
     * Parses the statement and returns an appropriate Statement.
     * @return
     */
    static Statement parse() {

        Statement s;
        Token t = scanner.getCurrentToken();
        scanner.buffer.skipBlanks();
        if (t instanceof IdToken && scanner.buffer.peek() == '(') {
            s = FunctionAssignStatement.parse();

        } else if (t instanceof IdToken) {
            s = AssignStatement.parse();

        } else if (t instanceof PrintToken) {
            s = PrintStatement.parse();

        } else if (t instanceof IfToken){
            s = IfStatement.parse();

        } else if (t instanceof WhileToken){
            s = WhileStatement.parse();

        } else if (t instanceof DoWhileToken){
            s = DoWhileStatement.parse();

        }else if(t instanceof MakeToken){
            s = MakeStatement.parse();
        }
        else{
            s=null;
        }
        
        return s;
    }

    @Override
    void print() {
     //To change body of generated methods, choose Tools | Templates.
    }
   
}
/**
 * Assign a value or Expression to an Id
 */
class AssignStatement extends Statement {
    Id id;
    Instruction expression;

    AssignStatement(Id id, Instruction instruction ){
        this.id = id;
        this.expression = instruction;
    }

    /**
     * Evaluates the expression to a Value and associates it with an Id
     * @return Value
     */
    @Override
    Value eval(){
        Value res = expression.eval();
        env_table.put(id.label,res );
        return new ValueNumber(0);
    }

    /**
     * Parses the Expression following the assignment operator,
     * then returns a new Assignment Statement with the Expression
     * @return AssignStatement
     */
    static public AssignStatement parse(){
        Id id = Id.parse();
        if(IdToken.isKeyword(id.label)) {
            Log.err("Err: " + id.label + " is a reserved keyword.\n\tCannot continue, please try again.\n");
            return null;
        }
        Token t = scanner.get(); //assignmentOpToken  or bracket here

        if(t instanceof AssignmentOpToken) {
            t = scanner.get();
            Expression e = Expression.parse();
            return new AssignStatement(id, e);
        }
        else if(t instanceof LeftBracketToken){
            t = scanner.get();
            Expression e = Expression.parse(); // get position
            t = scanner.get();
            if(t instanceof AssignmentOpToken){
                t = scanner.get();
                Expression exp = Expression.parse();
                t = scanner.getCurrentToken();
                return new AssignElementStatement(id, e, exp);
            }else{
                Log.err("Expecting := instead found "+t.getValue());
            }


        }
        return null;
    }
    
    @Override
    void print(){
        
    }
}

/**
 * Assign a value to an element in a list/vector.
 */
class AssignElementStatement extends AssignStatement{
    Expression element;

    AssignElementStatement(Id id, Expression element, Instruction e) {
        super(id, e);
        this.element = element;
    }

    /**
     * evaluates the expression and assigns the value to the element in the list/vector.
     * @return
     */
    @Override
    Value eval(){
        Value e = element.eval();
        ArrayList<Instruction> list  = env_table.get(id.label).getValues();
        if(!env_table.contains(id.label)){
            Log.err(id.label+" cannot be suscripted, value does not exist");
            return new ValueNumber(0);
        }
        list.set((int)e.eval().getValue(), expression.eval());
        env_table.put(id.label, new ValueVector(list) );

        return new ValueNumber(0);
    }
}

/**
 * Print an Expression.
 */
class PrintStatement extends Statement {
    Instruction instruction;
    
    PrintStatement( Instruction e ){
        this.instruction = e;
    }

    /**
     * Evaluates the epression and prints the value
     * @return Value
     */
    @Override
    Value eval(){
        Value result = instruction.eval();
        if(result == null){
            Log.err("returning null, most likely a logic/syntax error. Check code and run again");
        }
        if(result instanceof ValueNumber) {
            System.out.println(result);
            expression.Controller.appendConsole(
                    (Math.round(result.getValue() * 10000) / 10000.0) + "\n"
            );
        }
        else{
            expression.Controller.appendConsole("[ ");
            System.out.println(result);
            for(Instruction i : result.getValues()){
                expression.Controller.appendConsole(
                        (Math.round(i.eval().getValue() * 10000) / 10000.0) + " "
                );
            }
            expression.Controller.appendConsole("]\n");
        }
        return new ValueNumber(0);
    }

    static PrintStatement parse(){
        while(scanner.getCurrentToken() instanceof PrintToken){
            scanner.get();
        }
        Expression e = Expression.parse();
        return new PrintStatement(e);
    }

    @Override
    public String toString(){
        return "Print " + instruction.toString();
    }
    
}

/* IfStatement ::== <IfStatement>
*                || <IfStatement><ElseStatement>
*                || <IfStatement><ElseIfStatement>...
*                || <IfStatement><ElseifStatement>...<ElseStatement>
*
*  The sequence of if else and else if statements can become difficult to parse into different
*  classes/subclasses. Instead I chose to manage the chain of if, else and else if statements with
*  a List.  This is possible because you can easily treat each of the three statements as a pair
*  consisting of an Expression and either a Statement or Statement list, with else being represented
*  by an empty/null Expression value. By following this logic we can evaluate an IfStatement by
*  evaluating each Expression(condition) until one is greater than 0 or null, then executing it's
*  respective Statement/StatementList
 */
class IfStatement extends Statement {
    private LinkedList<Pair<Expression, Instruction >> ifElseChain = new LinkedList<>();

    IfStatement(LinkedList chain){
        ifElseChain = chain;
    }

    /**
     * Evaluates the If-Else Chain
     * @return
     */
    @Override
    Value eval(){
        Pair link = ifElseChain.removeFirst();
        if (((Expression) link.getKey()).eval().mag() > 0) {
            ((Instruction) link.getValue()).eval(); //statement or statement list
            return new ValueNumber(0);
        }
        while( !ifElseChain.isEmpty() ){
            link = ifElseChain.removeFirst();
            if ( link.getKey() == null ){
                ((Instruction) link.getValue()).eval(); //statement or statement list
                return new ValueNumber(0);
            }
            else if (((Expression) link.getKey()).eval().mag() > 0) {
                ((Instruction) link.getValue()).eval(); //statement or statement list
                return new ValueNumber(0);
            }
        }
        return new ValueNumber(0);
    }

    /**
     * Parses the If statement into corresponding links that make up an If-Else Chain.
     * @return IfStatement
     */
    static public IfStatement parse(){
        LinkedList<Pair> ifchain = new LinkedList<>();
        Token t = scanner.getCurrentToken();
        t = scanner.get(); // ( here
        t = scanner.get(); // Expression here
        Expression e = Expression.parse();
        t = scanner.get(); //// { or single statement
        boolean cont = false;

        do {
            // (1) get the first Statement/StatementList
            if (t instanceof StartBraceToken) { // brace, so might be multiple statements
                t = scanner.get(); // throw starting brace
                StatementList slist = StatementList.parse();
                ifchain.add(new Pair(e, slist));
                if (scanner.getCurrentToken() instanceof EndBraceToken)
                    t = scanner.get(); // throw ending brace
                else
                    Controller.appendConsole(">Error: Missing ending brace, found " + scanner.getCurrentToken().toString() + ".\n\tWill try to persist.\n");
            } else {  // no brace; safe to assume that there is a single statement
                Statement s = Statement.parse();
                ifchain.add(new Pair(e, s));
            }
            // (2) Check to see if there is an Else or Else If that follows, if so, continue loop after
            //     parsing the expression.
            if (scanner.getCurrentToken() instanceof ElseToken) {
                cont = true;
                t = scanner.get(); // { or statement/statementlist
                if (t instanceof IfToken) {
                    t= scanner.get();
                    t= scanner.get();
                    e = Expression.parse();
                    t= scanner.get();
                } else {
                    e = null;
                }
            }
            else
                cont = false;
        }while(cont == true);

        return new IfStatement(ifchain);
    }
}

/**
 * While Loop
 */
class WhileStatement extends Statement{
    private StatementList contents;
    private Expression condition;

    WhileStatement( Expression cond, StatementList sl ){
        condition = cond;
        contents = sl;
    }

    @Override
    Value eval(){
        while(condition.eval().mag() > 0){
            contents.eval();
        }
        return new ValueNumber(0);
    }

    static public WhileStatement parse(){
        Token t = scanner.get(); // get (
        t = scanner.get(); //Expression
        Expression e = Expression.parse();
        t = scanner.get(); // get {
        if( !(t instanceof StartBraceToken) )
            Controller.appendConsole(">Error: Expecting starting brace, found " + scanner.getCurrentToken().toString() + ".\n\tWill try to persist.\n");
        t = scanner.get(); // StatementList
        StatementList s = StatementList.parse();
        t = scanner.getCurrentToken();
        if( !(t instanceof EndBraceToken))
            Controller.appendConsole(">Error: Expecting ending brace, found " + scanner.getCurrentToken().toString() + ".\n\tWill try to persist.\n");
        t = scanner.get(); // throw away }
        return new WhileStatement(e, s);
    }

}

/**
 * Do While Loop
 */
class DoWhileStatement extends Statement{
    private StatementList contents;
    private Expression condition;

    DoWhileStatement( Expression cond, StatementList sl ){
        condition = cond;
        contents = sl;
    }

    @Override
    Value eval(){
        do{
            contents.eval();
        }while(condition.eval().mag() > 0);

        return new ValueNumber(0);
    }

    static public DoWhileStatement parse(){
        Token t = scanner.get(); // get {
        Expression e = null;
        if( !(t instanceof StartBraceToken) )
            Controller.appendConsole(">Error: Expecting starting brace, found " + scanner.getCurrentToken().toString() + ".\n\tWill try to persist.\n");
        t = scanner.get();
        StatementList s = StatementList.parse();
        t = scanner.getCurrentToken();
        if( !(t instanceof EndBraceToken))
            Controller.appendConsole(">Error: Expecting ending brace, found " + scanner.getCurrentToken().toString() + ".\n\tWill try to persist.\n");
        t = scanner.get();
        if(t instanceof WhileToken){
            scanner.get(); // ( here
            scanner.get();
            e = Expression.parse();
            scanner.get();
        }else{
            if( !(t instanceof StartBraceToken) )
                Controller.appendConsole(">Error: Expecting while token, found " + scanner.getCurrentToken().toString() + "\n" );
        }
        return new DoWhileStatement(e, s);
    }

}

/**
 * Assign an Expression to MFunction or assign a statementList to a Function (In development still).
 */
class FunctionAssignStatement extends AssignStatement{
    LinkedList<String> args=null;

    FunctionAssignStatement(Id id, Expression expression, LinkedList args) {
        super(id, expression);
        this.args = args;
    }
    static public FunctionAssignStatement parse(){
        Id id = Id.parse();
        if(IdToken.isKeyword(id.label))
            Controller.appendConsole("Err: "+id.label+" is a reserved keyword.\n\tCannot continue, please try again.\n");
        Token t = scanner.get(); // ( here
        LinkedList<String> arguments = new LinkedList<>();
        Token i;//get args
        do{
            i = scanner.get();
            arguments.add( Id.parse().label );
            i = scanner.get();
        }while (i instanceof ParamIterOpToken);
        scanner.get(); // throw away )
        // check for brace when adding program functions
        scanner.get(); // throw away :=
        Expression e = Expression.parse();
        return new FunctionAssignStatement_Math(id, e, arguments);
    }
    @Override
    Value eval(){
        Pair p = new Pair(expression, args);
        fenv_table.put(id.label, p);
        return new ValueNumber(0);
    }

}

class FunctionAssignStatement_Math extends FunctionAssignStatement{

    FunctionAssignStatement_Math(Id id, Expression expression, LinkedList args) {

        super(id, expression, args);
    }
}

/**
 * Make is called to create windows such as a graph
 */
class MakeStatement extends Statement {
    MakeStatement(){

    }
    @Override
    Value eval(){
        return new ValueNumber(0);
    }

    /**
     * determines the type of window to be created
     * @return MakeStatement
     */
    static public MakeStatement parse(){
        Token t = scanner.get(); // get component to make
        if(t instanceof GraphToken){
            t = scanner.get();
            if(t instanceof LeftParenToken){
                t=scanner.get();
                Id id = Id.parse();
                t = scanner.get();
                t = scanner.get();
                Expression l = Expression.parse();
                t = scanner.get();
                Expression u = Expression.parse();
                t = scanner.get();
                Expression i = Expression.parse();
                t = scanner.get();
                return new MakeGraphStatement(id, l, u, i);
            }else{
                Log.err("expected ( but instead found "+ t.value + "\n\tCannot continue, please try again.");
                return null;
            }
        }
        return new MakeStatement();
    }
}

/**
 * Make a graph window
 */
class MakeGraphStatement extends MakeStatement{
    Expression low, up, inc;
    Id fun;
    MakeGraphStatement(Id id, Expression l, Expression u, Expression inc){
        fun = id;
        low = l;
        up = u;
        this.inc = inc;
    }

    /**
     * Evaluates the function with a given range and creates a graph to represent the function.
     * @return Value
     */
    @Override
    Value eval() {
        Value res = null;
        double min = low.eval().getValue();
        double max = up.eval().getValue();
        double incr = inc.eval().getValue();
        double x = min;
        Pair p = fenv_table.get(fun.label);
        Expression e = (Expression) p.getKey();
        LinkedList argLabels = (LinkedList) p.getValue(); // x,y
        LinkedList<Value> temp = new LinkedList();
        HashMap<Double, Double> map = new HashMap<Double, Double>((int)(max-min));
        for (int i = 0; i < argLabels.size(); ++i) { // copy any values from within the environment already
            temp.add((Value) env_table.get((String) argLabels.get(i)));
        }
        XYChart.Series<Double, Double> series = new XYChart.Series<Double, Double>();
        while (x <= max) {
            for (int j = 0; j < argLabels.size(); ++j) { // put passed values into environment
                env_table.put((String) argLabels.get(j), new ValueNumber(x));
            }
            res = e.eval().eval();
            if(!Double.isNaN(res.getValue())) {
                
            }
            x = x + incr;
        }

        //Controller.chartData.add(series);
        for(int i=0; i<argLabels.size(); ++i){ // put back previous values into environment
            if(temp.get(i)!=null)
                env_table.put((String) argLabels.get(i), temp.get(i));
            else
                env_table.remove((String)argLabels.get(i));
        }
        //Controller.updateChart(min, max, incr, map);
        //Controller.xAxis = new NumberAxis((int)min, (int)max, incr);
        //Controller.yAxis = new NumberAxis((int)min, (int)max, incr);
        SyncChartList scl = new SyncChartList(Controller.chartData, map, min, max, incr);
        scl.run();
        return res;
    }
}