/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 *
 * @author morell
 */
public class Scanner {

    Token[] start;
    Buffer buffer;
    Token t;   // Most recently read token
    Scanner(Buffer b) {
        start = new Token[128];
        for (int i = 0; i < 128; i++) {
            start[i] = new BadToken();
            start[i].setBuffer(b);
        }
        buffer = b;
       
    }
    /**
     * Associate t with all the characters in chars -- the Strategy Pattern!
     * @param chars
     * @param t 
     */
    void init(String chars, Token t) {
        for (int i = 0; i < chars.length(); i++) {
            start[chars.charAt(i)] = t;
            t.setBuffer(buffer); // ensure every token uses the same buffer
        }
    }
    Token getCurrentToken() { return t;  }
    
    Token get() {
        buffer.skipBlanks();
        if (buffer.eof()) {
            t = new EofToken();  // this is what was missing
        } else {
            char ch = buffer.peek();
            t = start[ch].get();
        }
        return t;
    }
}
