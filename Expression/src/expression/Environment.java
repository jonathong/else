/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import java.util.HashMap;

/**
 * The environment class represents a table of Ids mapped to Values
 *
 */
public class Environment {
    private static HashMap<String,Value> table = new HashMap<>();

    public Environment () {
        if(table==null) {
            table = new HashMap<String, Value>();
        }
    }

    /**
     * returns the value corresponding to the id name/label.
     * @param id
     * @return Value
     */
    Value get(String id) {
        Value i = table.get(id);
        return i;
    }

    /**
     * associates the id with the given value in the table
     * @param id
     * @param val
     */
    void put (String id, Value val) {
        Value i = val;
        if(table.containsKey(id)){
            if( Controller.TRACE_FLAG )
                Controller.appendConsole("Debug#Trace: removing "+id +" from environment\n");
            table.remove(id);
        }
        table.put(id,i);

        if( Controller.TRACE_FLAG ){
            Controller.appendConsole("Debug#Trace: "+id + "="+i.toString()+"\n");
        }
    }

    /**
     * clears all the associates from the table
     */
    void clear(){
        if( Controller.TRACE_FLAG ){
            Controller.appendConsole("Debug#Trace: clearing environment \n");
        }
        table.clear();

    }
    boolean contains(String id){
        if(table.containsKey(id))
            return true;
        return false;
    }

    /**
     * removes the association of the id from the table.
     * @param id
     */
    void remove(String id){
        if( Controller.TRACE_FLAG )
            Controller.appendConsole("Debug#Trace: removing "+id +" from environment\n");
        table.remove(id);
    }

}