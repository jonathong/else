/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import java.util.ArrayList;

/**
 *
 * @author Jonathon
 * 
 * <StatementList> :: = <Statement> 
 *                |:: = <Statement> <StatementList>
 *
 *
 */

public class StatementList extends Instruction{
    ArrayList<Statement> s_list = new ArrayList<>();
    public StatementList(ArrayList list) {
        this.s_list = list;
    }


    /**
     * eval() calls the corresponding statement's eval method for each statement in the StatementList
     * @return Value
     */
    @Override
    Value eval(){
        for (Statement statement : s_list) {
            statement.eval();
        }
        //clear all environments
        return new ValueNumber(0);
    }

    /**
     * parse() parses statements until a token is found that does not match
     * @return StatementList
     */
    static StatementList parse(){
        ArrayList<Statement> list = new ArrayList<>();
        Token t = scanner.getCurrentToken();
        while(t instanceof IdToken || t instanceof StatementToken || t instanceof FunctionToken){
            list.add( Statement.parse() );
            t = scanner.getCurrentToken();
        }
        return new StatementList(list);
    }

    /**
     * prints each Statement in the StatementList.
     */
    @Override
    void print() {
        for (Statement statement : s_list) {
            statement.print();
        }
    }
}

