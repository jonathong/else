/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 * Id represents a label associated with a value
 * @author Jon
 */
public class Id extends Instruction {
    String label;
    
    public Id(String id){
        this.label = id;
    }

    /**
     * gets the value associated with the id label from the environment
     * @return Value
     */
    @Override
    Value eval() {
        return env_table.get( label );
    }

    /**
     * creates a new Id
     * @return
     */
    static Id parse() {
        Token t = scanner.getCurrentToken();
        Id result = null;
        if(t instanceof IdToken)
            result = new Id( t.getValue() ); // t.getValue() is null
        return result;
        
    }
    @Override
    void print() {
        System.out.println( label );
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(label);
        return sb.toString();
    }
}
