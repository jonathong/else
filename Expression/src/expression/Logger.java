package expression;


import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Jonathon on 4/22/2016.
 *
 */
public class Logger {

    public static final int LEVEL_FATAL = 0;
    public static final int LEVEL_ERROR = 1;
    public static final int LEVEL_WARNING = 2;
    public static final int LEVEL_INFO = 3;
    public static final int LEVEL_DEBUG = 4;

    private static Logger single;

    ArrayList<LogObserver> observers = new ArrayList();
    private static int levelFilter = 4;

    public static Logger getLogger(){
        if(single == null)
            single = new Logger();
        return single;
    }

    /**
     * sets the Logger to a certain Log Level
     * @param level
     */
    public static void setLevelFilter(int level){
        levelFilter = level;
    }

    public static int getLevelFilter(){
        return levelFilter;
    }

    /**
     * Objects can call this method to register with the logger and receive updates
     * @param o
     */
    public void register(LogObserver o) {
        if (o == null) {
            System.out.println("Error registering observer, observer null");
            return;
        }
        if (observers.contains(o)) {
            System.out.println("Error regisetering observer, already registered");
        }
        observers.add(o);

    }

    /**
     * Allows an object to remove it's registration with the logger.
     * @param o
     */
    public void unregister(LogObserver o){
        if(o == null){
            System.out.println("Error registering observer, observer null");
            return;
        }
        observers.remove(o);
    }

    public int observerCount(){
        return observers.size();
    }

    /**
     * Updates each of the objects in the observers list.
     * @param l
     */
    private void notifyObservers(LogMessage l){
        for(LogObserver o : observers){
            if(o instanceof LogObserver)
                o.update(l);
        }
    }

    /**
     * log a debug message.  Usually used for development purposes
     * @param message
     */
    public void debug(String message){
        if(LEVEL_DEBUG <= levelFilter)
            notifyObservers(new LogMessage("DEBUG",message ));
    }

    /**
     * Log something that the user may want to know
     * @param message
     */
    public void info(String message){
        if(LEVEL_INFO <= levelFilter)
            notifyObservers(new LogMessage("INFO",message ));
    }

    /**
     * Log a warning for the user
     * @param message
     */
    public void warn(String message){
        if(LEVEL_WARNING <= levelFilter)
            notifyObservers(new LogMessage("WARNING",message ));
    }

    /**
     * Log an error that the user caused.
     * @param message
     */
    public void err(String message){
        if(LEVEL_ERROR <= levelFilter)
            notifyObservers(new LogMessage("ERROR",message ));
    }

    /**
     * Log an error that the language caused.
     * @param message
     */
    public void fatal(String message){
        if(LEVEL_FATAL <= levelFilter)
            notifyObservers(new LogMessage("FATAL",message ));
    }


}

interface LogObserver {
    void update(LogMessage l);
}

class LogMessage {
    String level;
    String message;

    LogMessage(){
        this(null, null);
    }
    LogMessage(String l, String m){
        level = l;
        message = m;
    }
    public String getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }
    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        sb.append(level);
        sb.append("]");
        sb.append(message);
        sb.append("\n");
        return sb.toString();
    }
}