package expression;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.LinkedList;

/** MFunctionEnvironment  is similar to the normal Environment for maintaining user variables.
 *  This environment is used instead to maintain user Mathematical Functions; this struct holds
 *  the id of the function and a pair containing the expression of the function and a linked list
 *  with each of the mapped variables.
 *
 *
 * Created by Jonathon on 4/13/2016.
 */
public class FunctionEnvironment {
    private static HashMap<String, Pair< Instruction, LinkedList> > table = new HashMap<>();

    public FunctionEnvironment () {
        if(table==null) {
            table = new HashMap<String, Pair<Instruction, LinkedList>>();
        }
    }

    Pair get(String id) {
        Pair i = table.get(id);
        return i;
    }
    boolean contains(String id){
        if(table.containsKey(id))
            return true;
        return false;
    }

    void put (String id, Pair p) {
        table.put(id,p);
        if( Controller.TRACE_FLAG ){
            Controller.appendConsole("Debug#Trace: MFunction "+id + "() = "+p.toString()+"\n");
        }
    }
    void clear(){
        table.clear();
        if( Controller.TRACE_FLAG ){
            Controller.appendConsole("Debug#Trace: clearing function environment \n");
        }
    }

    void remove(String id){
        table.remove(id);
    }

}
