 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import static expression.PrintToken.getPrintToken;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.OutputStream;


 /**
 *
 * @author morell
 */
public class Main extends Application implements LogObserver{

    static void testScanner(Scanner scanner) {
      // Setup a buffer 

      Token t = scanner.get();
      while ( !(t instanceof BadToken) && !(t instanceof EofToken) ) {
         t = scanner.get();
      }
      if (!(t instanceof EofToken)) {
         System.out.println("Error should have been at end of file");
      }
   }

   static Scanner init(String input) {
      Buffer buffer = new Buffer(input);
      Scanner scanner = new Scanner(buffer);

      scanner.init( "0123456789.", NumberToken.getNumberToken() );
      scanner.init( "+", PlusOpToken.getPlusOpToken() );
      scanner.init( "-", MinusOpToken.getMinusOpToken() );
      scanner.init( "*", MultOpToken.getMultOpToken() );
      scanner.init( "/", DivideOpToken.getDivideOpToken() );
      scanner.init( "%", ModulusOpToken.getModulusOpToken() );
      scanner.init( "^", PowerOpToken.getPowerOpToken() );
      scanner.init( ">", GreaterThanOpToken.getGreaterThanOpToken() );
      scanner.init( "<", LessThanOpToken.getLessThanOpToken() );
      scanner.init( "=", EqualOpToken.getEqualOpToken() );
      scanner.init( "!", NotEqualOpToken.getNotEqualOpToken() );
      scanner.init( ">", GreaterThanEqualOpToken.getGreaterThanEqualOpToken() );
      scanner.init( "<", LessThanEqualOpToken.getLessThanEqualOpToken() );
      scanner.init( "|", OrOpToken.getOrOpToken() );
      scanner.init( "&", AndOpToken.getAndOpToken() );
      scanner.init( "(", LeftParenToken.getLeftParenToken() );
      scanner.init( ")", RightParenToken.getRightParenToken() );
      scanner.init( "[", LeftBracketToken.getLeftBracketToken() );
      scanner.init( "]", RightBracketToken.geRighttBracketToken() );
      scanner.init( ",", ParamIterOpToken.getParamIterOpToken() );
      scanner.init( "{", StartBraceToken.getStartBraceToken() );
      scanner.init( "}", EndBraceToken.getEndBraceToken() );
      scanner.init( ":", AssignmentOpToken.getAssignmentOpToken() );
      scanner.init( "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_#", new IdToken() );
      IdToken.registerKeyword("print", getPrintToken() );
      IdToken.registerKeyword("make", MakeToken.getMakeToken() );
      IdToken.registerKeyword("root", RootToken.getRootToken() );
      IdToken.registerKeyword("cos", CosToken.getCosToken() );
      IdToken.registerKeyword("sin", SinToken.getSinToken() );
      IdToken.registerKeyword("if", IfToken.getIfToken() );
      IdToken.registerKeyword("else", ElseToken.getElseToken() );
      IdToken.registerKeyword("while", WhileToken.getWhileToken() );
      IdToken.registerKeyword("do", DoWhileToken.getDoWhileToken() );
      IdToken.registerKeyword("mean", MeanToken.getMeanToken() );
      IdToken.registerKeyword("cross", CrossToken.getCrossToken() );
      IdToken.registerKeyword("dot", DotToken.getDotToken() );
      IdToken.registerKeyword("sCorr", SamCorrToken.getSamCorrToken() );
      IdToken.registerKeyword("pCorr", PopCorrToken.getPopCorrToken() );
      IdToken.registerKeyword("corr", CorrToken.getCorrToken() );
      IdToken.registerKeyword("pVar", PopVarianceToken.getPopVarianceToken() );
      IdToken.registerKeyword("sVar", SamVarianceToken.getSamVarianceToken() );
      IdToken.registerKeyword("pStdDev", PopStdDevToken.getPopStdDevToken() );
      IdToken.registerKeyword("sStdDev", SamStdDevToken.getSamStdDevToken() );
      IdToken.registerKeyword("sizeOf", SizeOfToken.getSizeOfToken() );
      IdToken.registerKeyword("mag", MagToken.getMagToken() );
      IdToken.registerKeyword("graph", GraphToken.getGraphToken() );
      return scanner;
   }
   static void testParser(Scanner scanner) {
      scanner.get(); // prime the pump
      Instruction.setScanner(scanner);
      StatementList instr = StatementList.parse();
      //System.out.println("Eval:" + instr.eval());
      instr.eval();
      // clear Environments
      Instruction.env_table.clear();
      Instruction.fenv_table.clear();
   }

   public static void main(String[] args) {
      //Scanner scanner = init("3*(4/3)*7/2/2");
      launch(args);
   }

   @Override
   public void start(Stage primaryStage) throws Exception {
       Parent root = FXMLLoader.load(getClass().getResource("Gui.fxml"));
       primaryStage.setTitle("ELSE - Write a Program!");
       primaryStage.setScene(new Scene(root, 800, 500));
       primaryStage.show();
      Instruction.Log.register(this);
   }
    @Override
    public void update(LogMessage l) {
         System.out.print(l.toString());
    }
 }
