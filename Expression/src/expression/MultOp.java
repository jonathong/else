/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import java.util.ArrayList;

/**
 *
 * @author morell
 */
public class MultOp extends Instruction {

   char operator;

   Value eval(Value l, Value r) {
      return new ValueNumber(0);
   }

   char get() {
      return 'c';
   }

   void print() {
      System.out.print(operator);
   }
}

class MultOpDivide extends MultOp {
   
   MultOpDivide() {
      operator = '/';
   }
   char get() {
      return operator;
   }

   Value eval(Value left, Value right) {
      if( (left instanceof ValueVector) && (right instanceof ValueVector) ){
         Log.err("Cannot divide with two vectors/lists,\n\treturning zero");
         return new ValueNumber(0);
      }else if(right instanceof ValueVector){ // right is a vector/list
         ArrayList<ValueNumber> res = new ArrayList<>(right.size());
         ArrayList<Instruction> r = right.eval().getValues();
         for(Instruction i : r){
            double rval = i.eval().getValue();
            if(rval==0){
               Log.warn("Dividing by zero is underfined, returning 0");
               res.add(new ValueNumber(0)); continue;
            }
            res.add( new ValueNumber(left.getValue()/rval));
         }
         return new ValueVector(res);
      }else if(left instanceof ValueVector){ //left is a vector/list
         ArrayList<ValueNumber> res = new ArrayList<>(right.size());
         ArrayList<Instruction> l = left.eval().getValues();
         for(Instruction i : l){
            double rval = right.getValue();
            if(rval==0){
               Log.warn("Dividing by zero is underfined, returning 0");
               res.add(new ValueNumber(0)); continue;
            }
            res.add( new ValueNumber(i.eval().getValue() / rval));
         }
         return new ValueVector(res);

      }
      if(right.getValue() == 0){
         Log.warn("Dividing by zero is undefined, returning zero");
         return new ValueNumber(0);
      }
      return new ValueNumber(left.getValue() / right.getValue());
   }

}

class MultOpMultiply extends MultOp {

   public MultOpMultiply() {
      operator = '*';
   }

   char get() {
      return operator;
   }

   Value eval(Value left, Value right) {
      if((left instanceof ValueVector) || (right instanceof ValueVector)) { // at least one is a vector
         ArrayList<Instruction> l = left.getValues();
         ArrayList<Instruction> r = right.getValues();
         ArrayList<ValueNumber> res = new ArrayList<>();

         if(left instanceof ValueNumber){ //scalar*vector
            for(int i=0; i<r.size();++i){
               double operand = r.get(i).eval().getValue();
               res.add( new ValueNumber( left.eval().getValue() * operand ) );
            }
            return new ValueVector(res);

         }else if(right instanceof ValueNumber){ //vector*scalar
            for(int i=0; i<l.size();++i){
               double operand = l.get(i).eval().getValue();
               res.add( new ValueNumber( operand * right.eval().getValue() ) );
            }
            return new ValueVector(res);

         }else{ // vector*vector
            if(left.size() == right.size()) { // cross product
               double result = 0;
               for (int i = 0; i < left.size(); ++i) {
                  result = result + ((ValueVector) left.eval()).getValue(i) * ((ValueVector) right.eval()).getValue(i);
               }
               return new ValueNumber(result);
            }
            else{
               Log.err("Vectors must be of the same dimension for cross/dot product");
            }
         }
      } // scalar * scalar
      return new ValueNumber(left.getValue()*right.getValue());
   }
}

class MultOpModulus extends MultOp {
   public MultOpModulus(){
      operator = '%';
   }
   char get(){ return operator;}
   Value eval(Value left, Value right) {
      if( (left instanceof ValueVector) && (right instanceof ValueVector) ){
         Log.err("Cannot use modulus with a vector,\n\treturning zero");
         return new ValueNumber(0);
      }
      else if (right instanceof ValueVector){ //right is a vector/list
         ArrayList<ValueNumber> res = new ArrayList<>(right.size());
         ArrayList<Instruction> r = right.eval().getValues();
         for(Instruction i : r){
            double rval = i.eval().getValue();
            if(rval==0){
               Log.warn("Dividing by zero is underfined, returning 0");
               res.add(new ValueNumber(0)); continue;
            }
            res.add( new ValueNumber(left.getValue()%rval));
         }
         return new ValueVector(res);
      }else if(left instanceof ValueVector){ //left is a vector/list
         ArrayList<ValueNumber> res = new ArrayList<>(right.size());
         ArrayList<Instruction> l = left.eval().getValues();
         for(Instruction i : l){
            double rval = right.getValue();
            if(rval==0){
               Log.warn("Dividing by zero is underfined, returning 0");
               res.add(new ValueNumber(0)); continue;
            }
            res.add( new ValueNumber(i.eval().getValue() % rval));
         }
         return new ValueVector(res);

      }

      return new ValueNumber(left.getValue() % right.getValue());
   }

}