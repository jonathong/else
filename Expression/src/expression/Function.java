package expression;

import java.util.ArrayList;

/**
 * Created by Jon on 4/7/2016.
 */
public class Function extends Instruction {
    Function(){
    }

    Value eval() {  /// override this
        return new ValueNumber(0);
    }

    /**
     * First parses the arguments, then returns a new Function subclass
     * that represents the token.
     * @return
     */
    static Function parse() {
        Function function = null;
        Token t = scanner.getCurrentToken();
        scanner.get();
        ArrayList<Instruction> args = new ArrayList<>();
        //function iterator?
        Token i;
        do {
            i = scanner.get();
            Expression e = Expression.parse();
            args.add(e);
            i = scanner.getCurrentToken();
        } while (i instanceof ParamIterOpToken);

        //TODO: lets try to speed this up soon.
        //      - We can create a Function that is the value for each token in a hashmap at startup.
        if (t instanceof RootToken)
            function = new Function_Root(args);
        else if (t instanceof CosToken)
            function = new Function_Cos(args);
        else if (t instanceof SinToken)
            function = new Function_Sin(args);
        else if (t instanceof CrossToken)
            function = new Function_Cross(args);
        else if (t instanceof DotToken)
            function = new Function_Dot(args);
        else if (t instanceof MeanToken)
            function = new Function_Mean(args);
        else if (t instanceof CorrToken)
            function = new Function_Corr(args);
        else if (t instanceof SamCorrToken)
            function = new Function_SamCorr(args);
        else if (t instanceof PopCorrToken)
            function = new Function_PopCorr(args);
        else if (t instanceof SamStdDevToken)
            function = new Function_SamStdDev(args);
        else if (t instanceof PopStdDevToken)
            function = new Function_PopStdDev(args);
        else if (t instanceof SamVarianceToken)
            function = new Function_SamVariance(args);
        else if (t instanceof PopVarianceToken)
            function = new Function_PopVariance(args);
        else if (t instanceof SizeOfToken)
            function = new Function_SizeOf(args);
        else if (t instanceof MagToken){
            function = new Function_Mag(args);
        }
        scanner.get();
        return function;
    }
    @Override
    void print() {
        //to do
    }
    public String toString() {
        return "";
    }
}

/**
 * returns the magnitude of the Value passed
 */
class Function_Mag extends Function{
    ArrayList<Instruction> args;
    Function_Mag(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        double mag=0;
        if(args.size() == 1){
            mag = args.get(0).eval().mag();
            return new ValueNumber(mag);
        }else{
            for(Instruction i : args){
                mag = mag + i.eval().getValue()*i.eval().getValue();
            }
            return new ValueNumber(Math.sqrt(mag));
        }
    }
}

/**
 * Returns the size of the Value/ValueVector passed
 */
class Function_SizeOf extends Function {
    ArrayList<Instruction> args;
    Function_SizeOf(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        if(args.size() == 1){
            return new ValueNumber(args.get(0).eval().size());
        }else{
            int size=0;
            for(Instruction i : args){
                size = size + i.eval().size();
            }
            return new ValueNumber(size);
        }
    }
}
/** r = sum(x*y)/sqrt(sum(x^2)*(sumy^2), where x = Xi - mean(X)
 *   = sum1/sqrt(sum2*sum3)
 *
 * Returns the product-moment correlation of the data passed.
 */
class Function_Corr extends Function{
    ArrayList<Instruction> args;
    Function_Corr(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        Value res=null;
        if(args.size()!=2) {
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }
        if(args.get(0).eval() instanceof ValueNumber || args.get(1).eval() instanceof ValueNumber){
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }

        ArrayList<Instruction> L1 = args.get(0).eval().getValues();
        ArrayList<Instruction> L2 = args.get(1).eval().getValues();
        int min = Math.min(L1.size(), L2.size());
        double x;
        double y;
        Function f = new Function_Mean(L1);
        double xm = f.eval().getValue();
        f = new Function_Mean(L2);
        double ym = f.eval().getValue();
        double sum1=0;
        double sum2=0;
        double sum3=0;
        for(int i=0;i<min;++i){
            x = L1.get(i).eval().getValue()-xm;
            y = L2.get(i).eval().getValue()-ym;
            sum1 = sum1 + (x*y);
            sum2 = sum2 + Math.pow(x,2);
            sum3 = sum3 + Math.pow(y,2);
        }
        res = new ValueNumber( (sum1)/(Math.sqrt( sum2 * sum3 )) );

      return res;
    }
}

/**
 * Returns the sample correlation of the data passed.
 */
class Function_SamCorr extends Function{
    ArrayList<Instruction> args;
    Function_SamCorr(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        Value res;
        if(args.size()!=2) {
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }
        if(args.get(0).eval() instanceof ValueNumber || args.get(1).eval() instanceof ValueNumber){
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }

        ArrayList<Instruction> L1 = args.get(0).eval().getValues();
        ArrayList<Instruction> L2 = args.get(1).eval().getValues();
        int min = Math.min(L1.size(), L2.size());

        Function f = new Function_Mean(L1);
        double xm = f.eval().getValue();
        f = new Function_Mean(L2);
        double ym = f.eval().getValue();
        ArrayList a = new ArrayList();
        a.add(args.get(0));
        f = new Function_SamStdDev(a);
        double Sx = f.eval().getValue();
        a.clear();
        a.add(args.get(1));
        f = new Function_SamStdDev(a);
        double Sy = f.eval().getValue();

        double sum=0;
        for(int i=0;i<min;++i){
            double x = L1.get(i).eval().getValue()-xm;
            double y = L2.get(i).eval().getValue()-ym;
            sum = sum + (x/Sx)*(y/Sy);
            System.out.println(sum);

        }
        sum = sum/(min-1);
        return new ValueNumber(sum );
    }

}

/**
 * Returns the population correlation of the data passed.
 */
class Function_PopCorr extends Function{
    ArrayList<Instruction> args;
    Function_PopCorr(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        Value res;
        if(args.size()!=2) {
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }
        if(args.get(0).eval() instanceof ValueNumber || args.get(1).eval() instanceof ValueNumber){
            Log.warn("Correlation requires two lists as arguments, returning 1");
            res = new ValueNumber(1);
            return res;
        }

        ArrayList<Instruction> L1 = args.get(0).eval().getValues();
        ArrayList<Instruction> L2 = args.get(1).eval().getValues();
        int min = Math.min(L1.size(), L2.size());

        Function f = new Function_Mean(L1);
        double xm = f.eval().getValue();
        f = new Function_Mean(L2);
        double ym = f.eval().getValue();
        ArrayList a = new ArrayList();
        a.add(args.get(0));
        f = new Function_PopStdDev(a);
        double Ox = f.eval().getValue();
        a.clear();
        a.add(args.get(1));
        f = new Function_PopStdDev(a);
        double Oy = f.eval().getValue();

        double sum=0;
        for(int i=0;i<min;++i){
            double x = L1.get(i).eval().getValue()-xm;
            double y = L2.get(i).eval().getValue()-ym;
            sum = sum + (x/Ox)*(y/Oy);
            System.out.println(sum);

        }
        sum = sum/(min);
        return new ValueNumber(sum );
    }
}
/**
 * Returns the population standard deviation of the data passed.
 */
class Function_PopStdDev extends Function{
    ArrayList<Instruction> args;
    Function_PopStdDev(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        if(args.size()!=1) {
            Log.err("pStdDev() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        if(args.get(0).eval() instanceof ValueNumber){
            Log.err("pStdDev() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        double variance = (new Function_PopVariance(args)).eval().getValue(); // get the mean
        return new ValueNumber(Math.sqrt(variance));
    }
}
/**
 * Returns the sample standard deviation of the data passed.
 */
class Function_SamStdDev extends Function{
    ArrayList<Instruction> args;
    Function_SamStdDev(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        if(args.size()!=1) {
            Log.err("sStdDev() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        if(args.get(0).eval() instanceof ValueNumber){
            Log.err("sStdDev() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        double variance = (new Function_SamVariance(args)).eval().getValue(); // get the mean
        return new ValueNumber(Math.sqrt(variance));
    }
}

/**
 * Returns the sample variance of the data passed
 */
class Function_SamVariance extends Function{
    ArrayList<Instruction> args;
    Function_SamVariance(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        if(args.size()!=1) {
            Log.err("sVar() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        if(args.get(0).eval() instanceof ValueNumber){
            Log.err("sVar() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        double mean = (new Function_Mean(args)).eval().getValue(); // get the mean
        ArrayList<Instruction> values = args.get(0).eval().getValues();
        double res=0;
        for(int i=0;i<values.size();++i){
             res = res + ( Math.pow( values.get(i).eval().getValue() - mean, 2 ) );
        }
        res = res / (values.size()-1);
        return new ValueNumber(res);
    }
}

/**
 * Returns the population Variance of the data passed
 */
class Function_PopVariance extends Function{
    ArrayList<Instruction> args;
    Function_PopVariance(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        if(args.size()>1 || args.size()<1) {
            Log.err("pVar() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        if(args.get(0).eval() instanceof ValueNumber){
            Log.err("pVar() requires one vector as an argument, returning 0");
            return new ValueNumber(0);
        }
        double mean = (new Function_Mean(args)).eval().getValue(); // get the mean
        ArrayList<Instruction> values = args.get(0).eval().getValues();
        double res=0;
        for(int i=0;i<values.size();++i){
            res = res + ( Math.pow( values.get(i).eval().getValue() - mean, 2 ) );
        }
        res = res / (values.size());
        return new ValueNumber(res);
    }
}

/**
 * Returns the dot product of two vectors
 */
class Function_Dot extends Function {
    ArrayList args;

    public Function_Dot(ArrayList args){
        if(args.size()!=2){
            Log.warn("Dot product requires two vectors, found "+args.size() + " parameters");
        }
        this.args = args;

    }
    @Override
    Value eval(){
        ArrayList<Instruction>left = ((Instruction)args.get(0)).eval().getValues();
        ArrayList<Instruction>right = ((Instruction)args.get(1)).eval().getValues();
        Value res = null;
        double val = 0;
        if(left.size()==right.size()){
            for(int i=0; i<left.size();++i){
                val = val + left.get(i).eval().getValue() * right.get(i).eval().getValue();
            }
            res = new ValueNumber(val);
        }else{
            Log.err("To compute the dot product both operands must be of the same dimensions");
        }
        return res;
    }
}

/**
 * Returns the cross product of two vectors of n dimensions
 */
class Function_Cross extends Function {
    ArrayList args;

    public Function_Cross(ArrayList args){
        if(args.size()!=2){
            Log.warn("Cross product requires two vectors, found "+args.size() + " parameters");
        }
        this.args = args;
    }
    @Override
    Value eval(){
        Value res = null;
        ArrayList<Instruction> left = ((Instruction)args.get(0)).eval().getValues();

        ArrayList<Instruction> right = ((Instruction)args.get(1)).eval().getValues();
        ArrayList<ValueNumber> result = new ArrayList();
        if(left.size()==right.size()){
            int d = left.size(); // dimension
            if(left.size() < 3){
                Log.info("Cannot create a plane ");
                result.add(new ValueNumber(0));
                result.add(new ValueNumber(0));
                result.add(new ValueNumber(0));
                res = new ValueVector(result);
                return res;
            }
            if(left.size()!=3 && left.size()!=7){
                Log.warn("The cross product only has the orthogonal property in 3 or 7 dimensions");
            }
            for(int i=0; i<d; ++i){
                result.add(new ValueNumber(
                        left.get( (i+1)%d).eval().getValue()
                        * right.get( (i+2)%d ).eval().getValue()
                        - left.get( (i+d-1)%d ).eval().getValue()
                        * right.get( (i+d-2)%d ).eval().getValue()
                ));
            }
            res = new ValueVector(result);
        }else{
            Log.err("To compute the dot product both operands must be of the same dimensions");
        }


        return res;
    }
}

/**
 * Returns the mean of ALL values passed
 */
class Function_Mean extends Function{
    ArrayList<Instruction> args;
    public Function_Mean(ArrayList args){
        this.args = args;
    }
    @Override
    Value eval(){
        Value res;
        int n = 0;
        double sum = 0;
        for (Instruction arg : args) {
            if (arg.eval() instanceof ValueVector) {
                ArrayList<Instruction> list = arg.eval().getValues();
                for (Instruction inst : list) {
                    sum = sum + inst.eval().getValue();
                    n++;
                }
                continue;
            }
            sum = sum + arg.eval().getValue();
            n++;
        }
        res = new ValueNumber( (sum/n) );
        return res;
    }
}

/**
 * returns the Nth root of the number, i.e. root(n, num) returns the n root of num.
 */
class Function_Root extends Function{
    Instruction n;
    Instruction num;
    public Function_Root(ArrayList args){
        if(args.size()!=2){
            Log.warn("Incorrect function call, \n\t usage: root(nthRoot, number)");
        }

        n = (Instruction)args.get(0);
        num = (Instruction)args.get(1);
    }
    @Override
    Value eval(){
        Value number = num.eval();
        Value res;
        if(number.mag() < 0)
            Log.warn("Square root of "+number +" is imaginary.\n\tImaginary numbers are not implemented.\n\tContinuing as if positive.");
        if(n instanceof ValueVector){ // invert the powers
            ArrayList<Instruction> p = new ArrayList<>(n.eval().size());
            for(Instruction i : n.eval().getValues()){
                p.add( new ValueNumber(1/(i.eval().getValue())) );
            }
            res = new ValueVector(p);
        }else{ //invert the power
            res = new ValueNumber( 1/( n.eval().getValue() ) );
        }
        Factor f = new FactorExponent( new FactorValue(num.eval()) , res );
        return f.eval();
    }
}

/**
 * Returns the value of the cos function, assumes radians
 */
class Function_Cos extends Function{
    Instruction num;
    public Function_Cos(ArrayList args){
        if(args.size()!=1){
            Log.warn("Incorrect function call. usage: cos(number),\n\t will use first parameter");
        }
        num = (Instruction) args.get(0);
    }
    @Override
    Value eval(){
        Value res;
        if(num.eval() instanceof ValueVector){
            ArrayList<Instruction> results = new ArrayList<>(num.eval().size());
            ArrayList<Instruction> nums = num.eval().getValues();
            for(int i=0; i<nums.size();++i){
                results.add( new ValueNumber(Math.cos( nums.get(i).eval().getValue() )));
            }
            res = new ValueVector(results);
        }
        else
            res = new ValueNumber( Math.cos(num.eval().getValue()) );
        return res;
    }
}

/**
 * Returns the value of the sin function, assumes radians.
 */
class Function_Sin extends Function{
    Instruction num;
    public Function_Sin(ArrayList args){
        if(args.size()!=1){
            Controller.appendConsole(">Warning: Incorrect function call, \n\t usage: sin(number),\n\t will try to persist.<\n");
        }
        num = (Instruction) args.get(0);
    }
    @Override
    Value eval(){
        Value res;
        if(num.eval() instanceof ValueVector){
            ArrayList<Instruction> results = new ArrayList<>(num.eval().size());
            ArrayList<Instruction> nums = num.eval().getValues();
            for(int i=0; i<nums.size();++i){
                results.add( new ValueNumber(Math.sin( nums.get(i).eval().getValue() )));
            }
            res = new ValueVector(results);
        }
        else
            res = new ValueNumber( Math.sin(num.eval().getValue()) );
        return res;
    }
}