/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 * Handles multiplication and Division between factors/terms
 * @author morell
 */
public abstract class MultEnd extends Instruction {

   public MultEnd() {
   }

   Value eval() {
      return new ValueNumber(0);
   }

   Value eval(Value left) {
      return new ValueNumber(0);
   }
   /* <multend> ::= <multop> <factor> | <multop> <factor> <multend> */

   static MultEnd parse() {
      MultOp multop = null;
      Token t = scanner.getCurrentToken();
      if (t instanceof DivideOpToken) {
         multop = new MultOpDivide();
      } else if (t instanceof MultOpToken) {
         multop = new MultOpMultiply();
      } else if (t instanceof ModulusOpToken){
         multop = new MultOpModulus();
      }
      scanner.get(); // Throw away the operator
      Factor factor = Factor.parse();

      t = scanner.getCurrentToken();
      MultEnd multend;
      if (t instanceof DivideOpToken || t instanceof MultOpToken || t instanceof ModulusOpToken) {
         multend = parse();
         multend = new MultEnd_MultOp_Factor_MultEnd(multop, factor, multend);
      } else {
         multend = new MultEnd_MultOp_Factor(multop, factor);
      }
      return multend;
   }

}

class MultEnd_MultOp_Factor extends MultEnd {

   MultOp mo;
   Factor f;

   public MultEnd_MultOp_Factor(MultOp mo, Factor f) {
      this.mo = mo;
      this.f = f;
   }

   Value eval(Value left) {
      char operator = mo.get();
      Value right = f.eval();
      return mo.eval(left, right);
   }

   @Override
   void print() {
      mo.print();
      f.print();
   }
   public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(mo.toString());
       sb.append(f.toString());
       return sb.toString();
   }
}

class MultEnd_MultOp_Factor_MultEnd extends MultEnd {

   MultOp mo;
   Factor f;
   MultEnd me;

   public MultEnd_MultOp_Factor_MultEnd(MultOp mo, Factor f, MultEnd me) {
      this.mo = mo;
      this.f = f;
      this.me = me;
   }

   Value eval(Value left) {
      char operator = mo.get();
      Value right = f.eval();
      Value result;
      MultOp m;
      if (operator == '*') {
         m = new MultOpMultiply();
         result = m.eval(left,right);
      } else if (operator == '/'){
         m = new MultOpDivide();
         result = m.eval(left,right);
      } else if (operator == '%') {
         m = new MultOpModulus();
         result = m.eval(left,right);
      }else{
         result = new ValueNumber(0);
      }
      return me.eval(result);
   }

   @Override
   void print() {
      mo.print();f.print(); me.print();
   }
      public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(mo.toString());
       sb.append(f.toString());
       sb.append(me.toString());
       return sb.toString();
   }

}
