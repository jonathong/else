package expression;

import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;


/**
 * Created by Jon on 3/19/2016.
 */
//needs work..
public class RichTextArea extends CodeArea {
    RichTextArea(){
        super("");
        init();

    }
    RichTextArea(String text){
        super(text);
        init();

    }
    private void init(){
        this.setParagraphGraphicFactory(LineNumberFactory.get(this));
        this.setEditable(true);

    }
}

class RichConsoleArea extends TextArea implements LogObserver {

    @Override
    public void update(LogMessage l) {
        this.appendText( l.toString() );
    }
}