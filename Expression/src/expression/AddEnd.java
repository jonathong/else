/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 *
 * @author Jonathon Gray
 */
public abstract class AddEnd extends Instruction{
    
    public AddEnd(){
        
    }
    
    Value eval(){
        return new ValueNumber(0);
    }
    
    Value eval(Value left) {
        return new ValueNumber(0);
    }
    
    static AddEnd parse() {
        AddOp addop = null;
        Token t = scanner.getCurrentToken();
        if (t instanceof MinusOpToken) {
            addop = new AddOpMinus();
        } else if (t instanceof PlusOpToken) {
            addop = new AddOpPlus();
        }
        scanner.get(); // Throw away the operator
        Term term = Term.parse();

        t = scanner.getCurrentToken();
        AddEnd addend;
        if (t instanceof MinusOpToken || t instanceof PlusOpToken) {
            addend = parse();
            addend = new AddEnd_AddOp_Factor_AddEnd(addop, term, addend);
        } else {
            addend = new AddEnd_AddOp_Factor(addop, term);
        }
        return addend;
    }
    
    @Override
    void print() {

    }
    
}

/**
 * AddEnd.parse creates a new AddEnd_AddOp_Factor when there is not another addend
 * following the factor.
 */
class AddEnd_AddOp_Factor extends AddEnd {

   AddOp ao;
   Term t;

   public AddEnd_AddOp_Factor(AddOp ao, Term t) {
      this.ao = ao;
      this.t = t;
   }

    /**
     * eval() evaluates the add or subtract operation
     * @param left
     * @return Value
     */
   Value eval(Value left) {
      char operator = ao.get();
      Value right = t.eval();
      return ao.eval(left, right);
   }

   @Override
   void print() {
      ao.print();
      t.print();
   }
   public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(ao.toString());
       sb.append(t.toString());
       return sb.toString();
   }
}

/**
 * AddEnd.parse creates a new AddEnd_AddOp_Factor_AddEnd when there is another addend
 * following the factor.
 */
class AddEnd_AddOp_Factor_AddEnd extends AddEnd {

   AddOp ao;
   Term t;
   AddEnd ae;

   public AddEnd_AddOp_Factor_AddEnd(AddOp ao, Term t, AddEnd ae) {
      this.ao = ao;
      this.t = t;
      this.ae = ae;
   }

    /**
     * eval() evaluates the add or subtract operations
     * @param left
     * @return Value
     */
   Value eval(Value left) {
       char operator = ao.get();
       Value right = t.eval();
       Value result;
       AddOp o;
       if (left instanceof ValueVector) {
           Log.info("Cannot add a real number to a vector, \n\t");
       }
           if (operator == '+') {
               o = new AddOpPlus();
               result = o.eval(left, right);
           } else {
               o = new AddOpMinus();
               result = o.eval(left, right);
           }
       return ae.eval(result);
   }

   @Override
   void print() {
      ao.print();t.print(); ae.print();
   }
      public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(ao.toString());
       sb.append(t.toString());
       sb.append(ae.toString());
       return sb.toString();
   }
}

