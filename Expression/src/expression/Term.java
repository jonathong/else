/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

/**
 *
 * @author morell
 */
public abstract class Term extends Instruction {

   Value eval() {  /// override this
      return new ValueNumber(0);
   }
   /*
    <term> :: = <factor> | <factor> <multend> 
    */

   static Term parse() {
      Factor factor;
      factor = Factor.parse();
      MultEnd multend;
      Term result;
      // Now check to see if there is a multiply or a divide operator next
      Token t = scanner.getCurrentToken();
      if (t instanceof DivideOpToken || t instanceof MultOpToken || t instanceof ModulusOpToken) {
         multend = (MultEnd) MultEnd.parse();
         result = new TermMultEnd(factor, multend);
      } else {
         result = new TermFactor(factor);
      }
      return result;
   }
}

class TermFactor extends Term {

   Factor factor;

   public TermFactor(Factor factor) {
      this.factor = factor;
   }
   @Override
   Value eval() {
      return factor.eval();
   }

   @Override
   void print() {
      factor.print();
   }
      public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(factor.toString());
       return sb.toString();
   }
}

class TermMultEnd extends Term {

   Factor f;
   MultEnd me;

   public TermMultEnd(Factor f, MultEnd me) {
      this.f = f;
      this.me = me;
   }
   
   @Override
   Value eval() {
      Value left = f.eval();
      return me.eval(left);
   }

   @Override
   void print() {
      f.print(); me.print();
   }
   public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(f.toString());
       sb.append(me.toString());
       return sb.toString();
   }
}
