/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expression;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Represents a Factor in an Expression
 * @author morell
 */
public class Factor extends Instruction {

   Factor() {
   }

   Value eval() {
      return null;
   }


   /** <Factor> ::= ( <Expression> ) | <Value> | <ValueVector> | <Id> | <function>
    *              | <Factor><Exponent>
    *
    * @return an appropriate Factor subclass.
     */
   static Factor parse() {
      Token t = scanner.getCurrentToken();
      Factor factor = null;

      if(t instanceof IdToken){
         Id id = Id.parse();
         Token i = scanner.get();
         factor = new FactorId(id);
         if(i instanceof LeftParenToken){   //must be a function call
            LinkedList<Expression> args = new LinkedList<>();
            do{
               i = scanner.get();
               args.add( Expression.parse());
               i = scanner.getCurrentToken();
            }while (i instanceof ParamIterOpToken);
            t = scanner.get();
            factor = new FactorMFunction(id, args);
         }else if(i instanceof LeftBracketToken) {
            t = scanner.get();
            Expression e = Expression.parse(); // get element
            t = scanner.get();
            factor = new FactorElement(id, e);
         }
      }else if (t instanceof LeftParenToken) {
         scanner.get();
         Expression exp = Expression.parse();
         t = scanner.getCurrentToken();
         if ( !(t instanceof RightParenToken) ) {
            System.err.println("Expected ')', found" + t);
            System.exit(1);
         } else {
            factor = new FactorExp(exp);
         }
         scanner.get(); // toss the rparen

      }else if(t instanceof LeftBracketToken) {
         Value v = Value.parse();
         factor = new FactorValue(v);

      }else if(t instanceof FunctionToken) {
         Function fun = Function.parse();
         factor = new FactorFunction(fun);
         t=scanner.getCurrentToken();

      }else{
         Value val = Value.parse();
         factor = new FactorValue(val);
         scanner.get(); // Toss it
      }
      //any factor can be exponentiated, so we best check that here
      t = scanner.getCurrentToken();
      if(t instanceof PowerOpToken){
         Instruction power;
         scanner.get();
         power = Factor.parse();
         factor = new FactorExponent(factor, power);
      }

      return factor;
   }

   @Override
   void print() {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }
}

/**
 * <Factor><Exponent>
 */
class FactorExponent extends Factor {
   Factor f;
   Instruction p;
   public FactorExponent(Factor factor, Instruction instruction) {
      f = factor;
      p = instruction;
   }

   /**
    * First evaluates the factor, then exponentiates the value
    * @return Value
     */
   public Value eval(){
      Value fValue = f.eval();
      Value pValue = p.eval();
      if( fValue instanceof ValueNumber) //2^2
         if(pValue instanceof ValueNumber){
            double fnum = fValue.getValue();
            double pnum = pValue.getValue();
            return new ValueNumber(Math.pow(fnum, pnum));
         }else{  // 2^[1,2,3]
            double fnum = f.eval().getValue();
            ArrayList<Instruction> elements = pValue.getValues();
            ArrayList<ValueNumber> res = new ArrayList<>(elements.size());
            for(Instruction e : elements){
               double p = e.eval().getValue();
               res.add( new ValueNumber( Math.pow(fnum, p) ) );
            }
            return new ValueVector(res);
         }
      else if(fValue instanceof ValueVector) {
         ArrayList<Instruction> fElem = ((ValueVector) fValue).getValues();
         ArrayList<ValueNumber> res = new ArrayList<>(fElem.size());
         if(pValue instanceof ValueNumber){ // [1,2,3]^2
            for(Instruction e : fElem){
               double f = e.eval().getValue();
               double p = pValue.eval().getValue();
               res.add( new ValueNumber( Math.pow( f, p ) ) );
            }
            return new ValueVector(res);
         }else{ // [1,2,3]^[1,2,3]
            ArrayList<Instruction> pElem = ((ValueVector) fValue).getValues();
            if(pElem.size() != fElem.size()){Log.err("Cannot exponentiate a vector with a vector of different dimensions"); return null; }
            for(int i=0; i<fElem.size(); ++i){
               double f = ((ValueNumber)fElem.get(i)).getValue();
               double p = ((ValueNumber)pElem.get(i)).getValue();
               res.add( new ValueNumber( Math.pow( f, p ) ) );
            }
            return new ValueVector(res);
         }
      }
      Log.fatal(" returning null at FactorExponent.eval()");
      return null;
   }

   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb.append(f.toString());
      sb.append("^");
      sb.append(p.toString());

      return sb.toString();
   }
}

/**
 * <Expression>
 */
class FactorExp extends Factor {
   Expression exp;
  
   public FactorExp(Expression e) {
      this.exp = e;
   }
   /**
    * Evaluates the expression.
    */
   public Value eval(){ // only temporary; modify to use exp later
      return exp.eval();
   }
   
   public String toString() {
       StringBuffer sb = new StringBuffer();
       //sb.append("Factor:= ");
       sb.append(exp.toString());
   
       return sb.toString();
   }
}

/**
 * <Value>
 */
class FactorValue extends Factor {

   Value value;

   FactorValue(Value v) {
      value = v;
   }
   @Override
   Value eval() {
      return value.eval();
   }
   
   public String toString() {
       StringBuffer sb = new StringBuffer();
       sb.append(value.toString());
       return sb.toString();
   }
}

/**
 * <ValueVectorElement>
 */
class FactorElement extends Factor{
   Expression element;
   Id id;
   FactorElement(Id i, Expression e) {
      this.element = e;
      this.id = i;
   }

   /**
    * gets the value from the vectorlist and returns the value
    * @return Value
     */
   @Override
   Value eval(){
      Value e = element.eval();
      ArrayList<Instruction> list  = env_table.get(id.label).getValues();
      if(e.eval().getValue() >= list.size()){
         Log.err("element subscript is larger than collection size,\n\tgetting last element instead");
         e = new ValueNumber(list.size()-1);
      }
      Value res = new ValueNumber( list.get( (int)e.eval().getValue() ).eval().getValue() );
      return res;
   }
   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb.append(id.label);
      sb.append("[");
      sb.append(element.toString());
      sb.append("]");
      return sb.toString();
   }
}

/**
 * <Id>
 */
class FactorId extends Factor {

   Id id;

   FactorId(Id id) {
      this.id = id;
   }
   @Override
   Value eval() {
      return id.eval();
   }
   
   public String toString() {
       StringBuffer sb = new StringBuffer();
       //sb.append("Factor:=");
       sb.append(id.toString());
       return sb.toString();
   }
}

/**
 * <Function>
 */
class FactorFunction extends Factor{
   Function function;
   FactorFunction(Function f){
      function = f;
   }

   /**
    * evalutes the functions and returns the value.
    * @return Value
     */
   @Override
   Value eval(){ return function.eval();}
   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb.append(function.toString());
      return sb.toString();
   }
}

/**
 * <MFunction>
 */
class FactorMFunction extends Factor{
   Id id;
   LinkedList<Expression> arguments; //actual arguments

   FactorMFunction(Id id, LinkedList args){
      this.id = id;
      this.arguments = args;
   }

   /**
    * Gets the Expression associated with the Id from the functionEnvironment, then
    * evaluates the Expression with the passed arguments.
    * @return Value
     */
   @Override
   Value eval(){
      Value res;
      Pair p = fenv_table.get(id.label);
      Expression e = (Expression) p.getKey();
      LinkedList argLabels = (LinkedList) p.getValue(); // x,y
      LinkedList<Value> temp = new LinkedList();

      for(int i=0; i<argLabels.size(); ++i){ // copy any values from within the environment already
         temp.add( (Value) env_table.get((String)argLabels.get(i)) );
      }
      for(int i=0; i<argLabels.size(); ++i){ // put passed values into environment
         env_table.put((String) argLabels.get(i), arguments.get(i).eval());
      }
      res = e.eval();
      for(int i=0; i<argLabels.size(); ++i){ // put back previous values into environment
         if(temp.get(i)!=null)
            env_table.put((String) argLabels.get(i), temp.get(i));
         else
            env_table.remove((String)argLabels.get(i));
      }
      return res;
   }
   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb.append(id.label);
      sb.append("(");
      for(Expression arg : arguments){
         sb.append(arg.toString());
      }
      sb.append(")");
      return sb.toString();
   }
}