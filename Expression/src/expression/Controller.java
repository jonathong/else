package expression;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * @author Created by Jon on 3/19/2016.
 *
 * Controller works as the middle-man between the FXML file and the associated code.
 * There are several methods here that are called when events such as button clicks occur.
 * The Initialize method will initialize certain components with default values in the app
 * and setup
 */
public class Controller implements Initializable {

    private static final String aboutUrl = "http://boole.cs.atu.edu/~jgray38/else/elsedoc.htm";

    static boolean TRACE_FLAG;
    @FXML
    private Button RunButton;
    @FXML
    private TabPane EditorPane;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem traceMenuItem;
    @FXML
    static private RichConsoleArea consoleText;
    @FXML
    private TitledPane consolePane;
    @FXML
    static public XYChart chart;

    static public Stage chartStage;
    static public ObservableList<XYChart.Series<Double,Double>> chartData;
    NumberAxis xAxis = new NumberAxis(-10, 10, 1);
    NumberAxis yAxis = new NumberAxis(-10,10, 1);
    @FXML
    private ComboBox comboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TRACE_FLAG = false;
        // Initialize EditorPane
        Tab tab = new Tab("Editor 1");
        tab.setContent( new RichTextArea() );
        tab.setClosable(true);
        EditorPane.getTabs().add(tab);
        // Initialize Console
        consoleText = new RichConsoleArea();
        Instruction.Log.register(consoleText);
        consoleText.setEditable(false);
        consoleText.setStyle("-fx-control-inner-background: black");
        ScrollPane s = new ScrollPane();
        s.setContent(consoleText);
        s.setStyle("-fx-control-inner-background: black");
        s.setFitToWidth(true);
        s.setPrefWidth(consolePane.getPrefWidth()-10);
        s.setPrefHeight(consolePane.getPrefHeight());
        consolePane.setContent(s);
        //consolePane
        // Iniitialize comboBox
        comboBox.getItems().addAll(
                "Fatal",
                "Error",
                "Warning",
                "Info",
                "Debug"
        );

        chartStage= new Stage();
        chartStage.setTitle("ELSE - Graph View");
        chart = new LineChart<Number, Number>(xAxis, yAxis);
        chart.setLegendVisible(false);
        chartData = FXCollections.synchronizedObservableList(FXCollections.observableList(new ArrayList<XYChart.Series<Double, Double>>()));
        chartData.addListener((ListChangeListener<XYChart.Series<Double, Double>>) c -> {
            //chartStage.hide();
            chart.getData().removeAll();
            chart.getData().addAll(chartData);
            //chartStage.setScene(new Scene(chart));
            chartStage.show();
        });
        chart.getData().addAll(chartData);
        Button b = new Button("remove function");
        b.setOnAction(e-> {
            for (int i = 0; i < chart.getData().size(); ++i) {
                chart.getData().remove(i);
            }
        });
        chartStage.setScene(new Scene(new VBox(chart, b)));
        chartStage.getScene().getStylesheets().add(Expression.class.getResource("invisible.css").toExternalForm());
        //chartStage.show();

    }

    static public void appendConsole(String add){
        SyncConsoleText sct = new SyncConsoleText(add, consoleText);
        sct.run();
    }

    public void clearConsole(){
        consoleText.setText("");
    }


    /**
     * @author Jonathon Gray
     *
     * Runs the instructions from the console in a new thread.
     */
    public void runButtonClicked(){

        Runnable r = () -> {
            String instr = ((RichTextArea) EditorPane.getSelectionModel().getSelectedItem().getContent()).getText();
            Scanner scanner = Main.init(instr);
            //testScanner(scanner);
            Main.testParser(scanner);
        };
        new Thread(r).start();
    }

    public void newMenuItemClicked(){
        Tab tab = new Tab("Editor "+ (EditorPane.getTabs().size()+1) );
        tab.setContent(new RichTextArea());
        tab.setClosable(true);
        EditorPane.getTabs().add(tab);
    }

    public void renameMenuItemClicked(){
        Tab tab = EditorPane.getSelectionModel().getSelectedItem();
        Stage window = new Stage();
        VBox box = new VBox(10);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Choose new name");

        TextField field = new TextField(tab.getText());

        Button done = new Button("rename");
        done.setOnAction(e -> {
            tab.setText( field.getText() );
            window.close();
        });

        box.getChildren().addAll(field, done);
        Scene scene = new Scene(box);
        window.setScene(scene);
        window.showAndWait();
    }

    public void traceMenuItemClicked(){
        if(TRACE_FLAG){
            traceMenuItem.setText("Trace ☐");
            TRACE_FLAG = false;
        }else{
            traceMenuItem.setText("Trace ☑");
            TRACE_FLAG = true;
        }
    }

    public void openMenuItemClicked() throws IOException {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open an ELSE program");
        fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        List<File> fList = fc.showOpenMultipleDialog(null);

        if(fList != null){
            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();
            String line = new String();
            for(File f : fList){
                try{
                    br = new BufferedReader(new FileReader(f.getPath()));
                    while( (line = br.readLine()) != null ){ sb.append(line+"\n");}
                    Tab tab = new Tab( f.getName() );
                    tab.setContent(new RichTextArea( sb.toString() ));
                    tab.setClosable(true);
                    EditorPane.getTabs().add(tab);
                }catch (FileNotFoundException e){
                    appendConsole("Err: File not found, check file path \n");
                }
            }
        }else{
            appendConsole("Err: File could not be opened, check file path. \n");
        }
    }
    public void onAboutMenuItemClicked(){
        Stage stage = new Stage();
        WebView wv = new WebView();
        wv.getEngine().load(aboutUrl);
        Scene s = new Scene(wv);
        stage.setScene(s);
        stage.setTitle("User Manual");
        stage.show();
    }

    public void saveMenuItemClicked() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Save an ELSE program");
        fc.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        fc.setInitialFileName(EditorPane.getSelectionModel().getSelectedItem().getText() + ".txt");
        File f = fc.showSaveDialog(null);
        if(f!=null) {
            String name = f.getName();
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(f.getPath()));
                bw.write(((RichTextArea) EditorPane.getSelectionModel().getSelectedItem().getContent()).getText());
                bw.close();
            } catch (IOException e) {
                appendConsole("Err: File not saved, try again\n");
            }
        }else{
            // User decided not to save the file. Do nothing.
        }
    }
    public void onLogLevelAction(){
        if(comboBox.getValue()=="Fatal"){
            Logger.setLevelFilter(Logger.LEVEL_FATAL);

        }else if(comboBox.getValue()=="Error"){
            Logger.setLevelFilter(Logger.LEVEL_ERROR);

        }else if(comboBox.getValue() == "Warning"){
            Logger.setLevelFilter(Logger.LEVEL_WARNING);

        }else if(comboBox.getValue() == "Info"){
            Logger.setLevelFilter(Logger.LEVEL_INFO);

        }else if(comboBox.getValue() == "Debug"){
            Logger.setLevelFilter(Logger.LEVEL_DEBUG);

        }else{
            Controller.appendConsole("Error selecting Log Threshold");
        }
    }

}

/**
 * Used to execute code on the application thread that adds data to the ObservableList
 *
 * Platform.runLater() is what makes this possible, the passed data can be added to the list from here.
 */
class SyncChartList implements Runnable {
    ObservableList<XYChart.Series<Double,Double>> data;
    HashMap map;
    double min, max, inc;
    ScatterChart s;
    Stage stage;
    SyncChartList(ObservableList<XYChart.Series<Double, Double>> list, HashMap map, double min, double max, double inc){
        data = list;
        this.map = map;
        this.inc = inc;
        this.min = min;
        this.max = max;
        this.s = s;
    }
    @Override
    public void run() {
        Platform.runLater(()-> {
            double thisone = min;
            XYChart.Series<Double, Double> series = new XYChart.Series<Double, Double>();
            while (thisone <= max) {
                series.getData().add(new XYChart.Data(new Double(thisone), map.get(thisone)));
                thisone = thisone + inc;
            }
            series.setNode(new Rectangle(0,0));
            data.clear();
            data.add(series);
            //s.getData().add(data);
        });
    }
}

class SyncConsoleText implements Runnable{
    TextArea t;
    String m;
    SyncConsoleText(String message, TextArea t){
        this.t = t;
        this.m = message;
    }
    @Override
    public void run() {
        Platform.runLater(()->{
            t.appendText(m);
        });
    }
}